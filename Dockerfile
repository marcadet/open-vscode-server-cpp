
FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/open-vscode-server-base:latest

## to get permissions to install packages and such
USER root

## software needed

# g++-12 is available on Ubuntu 22.04, but boost currently depends on g++-11
RUN apt-get update && apt-get install -y --no-install-recommends \
        clangd \
        cmake \
        gdb \
        g++-11 \
        googletest \
        libboost-dev \
        libtbb-dev \
        make \
        valgrind \
    && rm -rf /var/lib/apt/lists/*

RUN update-alternatives \
  --install /usr/bin/gcc                 gcc                  /usr/bin/gcc-11     100 \
  --slave   /usr/bin/g++                 g++                  /usr/bin/g++-11 \
 && update-alternatives \
  --install /usr/bin/cc                  cc                   /usr/bin/gcc        100 \
 && update-alternatives \
  --install /usr/bin/c++                 c++                  /usr/bin/g++        100

## Build googletest with installed compiler
RUN mkdir -p /tmp/gtest \
    && cd /tmp/gtest \
    && cmake /usr/src/googletest/googletest \
    && make all \
    && mv /tmp/gtest/lib/* /usr/lib/ \
    && mv /usr/src/googletest/googletest/include/gtest /usr/include/ \
    && rm -r /tmp/gtest

# script updated
COPY wrapper_script.sh /usr/local/lib/wrapper_script.sh

## user installations
USER openvscode-server

# C++ extension

# Seems complicated to configure globally for c++20 (see https://clang.llvm.org/docs/JSONCompilationDatabase.html)
# RUN /home/.openvscode-server/bin/openvscode-server \
#         --extensions-dir /home/initialWorkspace/.openvscode-server/extensions \
#         --install-extension llvm-vs-code-extensions.vscode-clangd

# ms-vscode.cpptools not found (and not available in extension view)
# RUN /home/.openvscode-server/bin/openvscode-server \
#         --extensions-dir /home/initialWorkspace/.openvscode-server/extensions \
#         --install-extension ms-vscode.cpptools

RUN     curl -L -o /tmp/cpptools-linux.vsix https://github.com/microsoft/vscode-cpptools/releases/download/v1.11.5/cpptools-linux.vsix \
    && /home/.openvscode-server/bin/openvscode-server \
        --extensions-dir /home/initialWorkspace/.openvscode-server/extensions \
        --install-extension /tmp/cpptools-linux.vsix \
    && rm /tmp/cpptools-linux.vsix \
    && chmod +x /home/initialWorkspace/.openvscode-server/extensions/ms-vscode.cpptools-1.11.5/bin/cpptools \
    && chmod +x /home/initialWorkspace/.openvscode-server/extensions/ms-vscode.cpptools-1.11.5/bin/cpptools-srv

RUN mkdir -p /home/initialWorkspace/.vscode
COPY c_cpp_properties.json /home/initialWorkspace/.vscode/
COPY            tasks.json /home/initialWorkspace/.vscode/
COPY           launch.json /home/initialWorkspace/.vscode/

# some other folders in VSCode explorer vue to hide
COPY settings.json /home/initialWorkspace/.openvscode-server/data/Machine/

# BUT debug (breakpoints) does nor work!
